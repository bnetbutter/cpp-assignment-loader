from setuptools import setup

setup(
    name="cs1570",
    entry_points={
        "console_scripts": [
            "cs1570=scripts:main",
            "cs1570-format=scripts:format_cpp",
            "cs1570-header=scripts:header_comment",
        ]
    }
)