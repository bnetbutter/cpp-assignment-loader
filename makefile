# Generated from https://gitlab.com/bnetbutter/cpp-assignment-loader
SOURCES := $(wildcard src/*.cpp)
ASGMT_SRC := $(SOURCES:src/%=%)

all: $(ASGMT_SRC)

%.cpp: src/%.cpp
	{ $(shell echo -e cs1570-header $@$(cat $<)); cat $<; } | cs1570-format > $@

autoformat:
	watchmedo auto-restart -p "src/*.cpp" -R make

autorun:
	cd src && python3 autorun.py

build:
	make autoformat & make autorun