# cpp-assignment-loader
Initialize cs1570 assignment submission pipeline. 

Setup
```bash
$ python3 setup.py install --user
```

First time usage will prompt for user info
```bash
$ cs1570
```

Every time thereafter, this clones your homework repo into the current dir.
```
$ cs1570 hw1 
$ cd hw1
$ make build
```

The last command starts file watchers in `hw` and `src` directories and
which automates compilation, running, and copying formatted cpp source code to
the root assignment directory.