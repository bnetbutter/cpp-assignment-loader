#   $ cs1570-format
def format_cpp():
    import re

    # Match a function definition
    function_def = r"(?<=\n)(([a-z]|_|~.*|[0-9])*\((.*)\))"

    # Match a bracket
    bracket = re.compile(r"(?<=.)\{")
    # Match whitespace up to first non-whitespace char
    indent = re.compile(r"^(\s*)")

    # Read in our input from stdin
    src = ""
    while True:
        try:
            src += input() + "\n"
        except EOFError:
            break

    lines = [] 
    for line in src.splitlines():    
        match_bracket = bracket.search(line)
        match_indent = indent.search(line)
        if match_bracket is not None and match_indent is not None:
            # Match bracket and get indent level. Replace the bracket with
            # bracket that starts/ends on the same line
            repl = "\n" + "    " * match_indent.end() + "{"
            lines.append(bracket.sub(repl, line, 1))
        else:
            lines.append(line)

    src = "\n".join(lines)

    # Match function def and put name on same line as return type
    mutable = bytearray(src, encoding="utf-8")
    for match in re.finditer(function_def, src, re.MULTILINE):
        mutable[match.start() - 1] = ord(' ')

    # Output result to stdout
    print(mutable.decode().replace("    ", "  "))

def header_comment():
    import os
    import json
    from datetime import datetime
    import sys
    import textwrap

    HEADER_FILE_FORMAT = textwrap.dedent("""
    // Programmer: {name}
    // Date: {date}
    // File: {file}
    // Class: CS1570
    // Section: {section}
    // Description: <DESCRIPTION HERE>
    """).strip()

    with open(os.path.join(os.environ["HOME"], ".cs1570"), "r") as fp:
        data = json.load(fp)
    
    print(HEADER_FILE_FORMAT.format(
        name=data["name"],
        date=datetime.today().strftime("%m/%d/%Y"),
        file=sys.argv[1],
        section=data["section"] 
    ))


#   $ cs1570
def main():
    import argparse
    import os
    import subprocess
    import json
    
    GRP_FORMAT="{year}-{semester}-CS1570-{section}"
    ASGMT_FORMAT = "{year}-{semester}-{section}-{assignment}-{user}"
    URL="https://git-classes.mst.edu"

    parser = argparse.ArgumentParser()
    if os.path.exists(path := os.path.join(
        os.environ["HOME"], ".cs1570"
    )):
        
        with open(path) as fp:
            data = json.load(fp)
            
        parser.add_argument("assignment")
        args = parser.parse_args()
        clone_url = URL + "/" + GRP_FORMAT.format(
            year=data["year"],
            semester=data["semester"],
            section=data["section"]
        ) + "/" + ASGMT_FORMAT.format(
            year=data["year"],
            semester=data["semester"],
            section=data["section"],
            assignment=args.assignment,
            user=data["user"],
        )
        if subprocess.call(["git", "clone", clone_url, args.assignment]):
            print("clone failed") 
            return 1
        
        os.chdir(args.assignment)
        if subprocess.call(["git", "clone", 
                "https://gitlab.com/classroomcode/"
                    "vsce-assignment/cpp-run-on-save",
                "src",
            ]
        ):
            print("clone failed")
            return 1
        
        os.chdir("src")
        subprocess.call(["rm", "-rf", ".git"])
        os.chdir("../")
        src_makefile = os.path.join(os.path.dirname(os.path.abspath(__file__)), "makefile")
        subprocess.call(["cp", src_makefile, "makefile"])
    else:
        data = {
            "year": input("year: "),
            "semester": input("semester: "),
            "section": input("section: "),
            "user": input("id: "),
            "name": input("name: "),
        }
        with open(os.path.join(os.environ["HOME"], ".cs1570"), "w") as fp:
            json.dump(data, fp)
        
